import numpy as np
from scipy.stats import rankdata, ranksums

from tabulate import tabulate

clfs = [
    "mmc-D1",
    "mmc-D2",
    "mmc-R1",
    "mmc-M1",
    "mmc-M2",
    "mdd-D1",
    "mdd-D2",
    "mdd-R1",
    "mdd-M1",
    "mdd-M2",
    "ct-D1",
    "ct-D2",
    "ct-R1",
    "ct-M1",
    "ct-M2",
]

scores = np.load('compare-20220606-001238.npy')
# print(scores)
avg_sc = scores.mean(axis=1)
print("\nMean scores:\n", avg_sc)
avgs = scores.T
# print(avgs)

ranks = []
for ms in avgs:
    ranks.append(rankdata(ms).tolist())

ranks = np.array(ranks)
print(ranks)

mean_ranks = np.mean(ranks, axis=0)
print("\nMean ranks:\n", mean_ranks)

alpha = .05

w_stats = np.zeros((len(clfs), len(clfs)))
p_vals = np.zeros((len(clfs), len(clfs)))

for i in range(len(clfs)):
    for j in range(len(clfs)):
        w_stats[i, j], p_vals[i, j] = ranksums(ranks.T[i], ranks.T[j])

names_column = np.expand_dims(np.array(clfs), axis=1)
w_statistic_table = np.concatenate((names_column, w_stats), axis=1)
w_statistic_table = tabulate(w_statistic_table, clfs, floatfmt=".2f")
p_value_table = np.concatenate((names_column, p_vals), axis=1)
p_value_table = tabulate(p_value_table, clfs, floatfmt=".2f")
print("\nw-statistic:\n", w_statistic_table, "\n\np-value:\n", p_value_table)

advantage = np.zeros((len(clfs), len(clfs)))
advantage[w_stats > 0] = 1
advantage_table = tabulate(np.concatenate(
    (names_column, advantage), axis=1), clfs)
print("\nAdvantage:\n", advantage_table)

significance = np.zeros((len(clfs), len(clfs)))
significance[p_vals <= alpha] = 1
significance_table = tabulate(np.concatenate(
    (names_column, significance), axis=1), clfs)
print("\nStatistical significance (alpha = 0.05):\n", significance_table)

