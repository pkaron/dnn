import glob
import pickle
from datetime import datetime
import os

import seaborn as sns
import librosa
import soundfile
from matplotlib import pyplot as plt
from sklearn import clone
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler, normalize

# stderr = sys.stderr
# sys.stderr = open(os.devnull, 'w')

import numpy as np

from sklearn.metrics import accuracy_score, confusion_matrix
from tensorflow.keras.models import Sequential
from tensorflow.python.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.python.keras.layers import LSTM, Dropout, Dense
from tensorflow.python.keras.models import load_model, clone_model
from tensorflow.python.keras.regularizers import L2
from tensorflow.python.keras.utils.np_utils import to_categorical

from data_utils import load_data, prepare_data_tens, prepare_data_cl, prep_tens

EMOTIONS_MAP = {
    "01": "neutral", "02": "calm", "03": "happy", "04": "sad",
    "05": "angry", "06": "fearful", "07": "disgust", "08": "suprised"
}

AVAILABLE_EMOTIONS = {
    "neutral",
    "happy",
    "sad",
    "angry",
    "calm",
    "fearful",
}
int2emotions = {i: e for i, e in enumerate(AVAILABLE_EMOTIONS)}
emotions2int = {v: k for k, v in int2emotions.items()}


def build_model_rec(
        input_length,
        n_rnn_layers=1,
        n_dense_layers=2,
        rnn_units=64,
        dense_units=128,
        cell=LSTM,
        dropout_mlt=0.3,
        output_dim=len(AVAILABLE_EMOTIONS),
        loss="categorical_crossentropy",
        optimizer="adam", ):
    if n_rnn_layers < 1:
        raise Exception("n_rnn_layers must be >= 1")

    dropout = [dropout_mlt] * (n_rnn_layers + n_dense_layers)

    model = Sequential()

    model.add(cell(rnn_units, return_sequences=True, input_shape=(None, input_length)))
    model.add(Dropout(dropout[0]))

    i = 0
    for i in range(1, n_rnn_layers):
        model.add(cell(rnn_units, return_sequences=True))
        model.add(Dropout(dropout[i]))

    for j in range(n_dense_layers):
        model.add(Dense(dense_units))
        model.add(Dropout(dropout[i + j]))

    # Classification
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def build_model_dense(
        input_length,
        output_dim=(len(AVAILABLE_EMOTIONS)),
        loss="categorical_crossentropy",
        optimizer="adam"):
    model = Sequential()
    model.add(Dense(256, kernel_regularizer=L2(l2=1e-4), input_shape=(None, input_length)))
    model.add(Dense(512, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(64, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def build_model_dense2(
        input_length,
        output_dim=(len(AVAILABLE_EMOTIONS)),
        loss="categorical_crossentropy",
        optimizer="adam"):
    model = Sequential()
    model.add(Dense(64, kernel_regularizer=L2(l2=1e-4), input_shape=(None, input_length)))
    model.add(Dense(64, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(128, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(64, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def train(_model, model_name, _data, batch_size=64, epochs=300):
    X_train, X_test, y_train, y_test = _data
    model_filename = "results/" + model_name + datetime.now().strftime("%Y%m%d-%H%M%S")
    checkpointer = ModelCheckpoint(model_filename, save_best_only=True, verbose=1)

    log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

    return _model.fit(
        X_train, y_train,
        batch_size=batch_size,
        epochs=epochs,
        validation_data=(X_test, y_test),
        callbacks=[tensorboard_callback, checkpointer],
        verbose=True
    )


def test_score_tens(_model, _data, emotions):
    X_train, X_test, y_train, y_test = _data
    y_test = y_test[0]
    y_pred = _model.predict(X_test)[0]
    y_pred = [np.argmax(y, out=None, axis=None) for y in y_pred]
    y_test = [np.argmax(y, out=None, axis=None) for y in y_test]

    cff = confusion_matrix(y_test, y_pred)
    score = accuracy_score(y_true=y_test, y_pred=y_pred)
    print(f"Accuracy: {score}%")
    cm = sns.light_palette("seagreen", as_cmap=True)
    sns.heatmap(cff, annot=True, fmt='d', cmap=cm, xticklabels=emotions, yticklabels=emotions)
    plt.show()
    return score


def test_score_cl(_model, _data, emotions):
    X_train, X_test, y_train, y_test = _data
    y_pred = _model.predict(X_test)
    # y_pred = [np.argmax(y, out=None, axis=None) for y in y_pred]
    # y_test = [np.argmax(y, out=None, axis=None) for y in y_test]

    cff = confusion_matrix(y_test, y_pred)

    cff_s = normalize(cff)

    score = accuracy_score(y_true=y_test, y_pred=y_pred)
    print(f"Accuracy: {score}%")
    cm = sns.light_palette("seagreen", as_cmap=True)
    sns.heatmap(cff, annot=True, fmt='d', cmap=cm, xticklabels=emotions, yticklabels=emotions)
    plt.show()

    cm = sns.light_palette("seagreen", as_cmap=True)
    sns.heatmap(cff_s, annot=True, cmap=cm, xticklabels=emotions, yticklabels=emotions)
    plt.show()
    return score


def mod_dense(kwargsss):
    emots = AVAILABLE_EMOTIONS
    X, y = load_data(emots, EMOTIONS_MAP, kwargsss)
    data = prepare_data_tens(X, y, emotions2int, test_size=0.15, normalize=True)
    model = build_model_dense(X.shape[1], output_dim=len(emots))
    hist = train(model, "dense-mfcc-dmfcc-ddmfcc", data)
    print(hist)
    print(model.summary())
    ts = test_score_tens(model, data, emots)
    print("Test accuracy score:", ts * 100, "%")


def mod_rec(kwargsss):
    emots = AVAILABLE_EMOTIONS
    X, y = load_data(emots, EMOTIONS_MAP, **kwargsss)
    data = prepare_data_tens(X, y, emotions2int, test_size=0.15, normalize=True)
    model = build_model_rec(input_length=X.shape[1], output_dim=len(emots))
    hist = train(model, "rec-mfcc-dmfcc-ddmfcc", data)
    print(hist)
    print(model.summary())
    ts = test_score_tens(model, data, emots)
    print("Test accuracy score:", ts * 100, "%")


def mod_cl(kwargsss):
    emots = AVAILABLE_EMOTIONS
    X, y = load_data(emots, EMOTIONS_MAP, kwargsss)
    data = prepare_data_cl(X, y, test_size=0.15, normalize=True, random_state=421)
    X_train, X_test, y_train, y_test = data
    model_params = {
        'alpha': 0.01,
        'batch_size': 256,
        'epsilon': 1e-08,
        'hidden_layer_sizes': (128, 256, 64,),
        'learning_rate': 'adaptive',
        'max_iter': 500,
    }
    model = MLPClassifier(**model_params)
    model.fit(X_train, y_train)
    ts = test_score_cl(model, data, emots)
    print("Test accuracy score:", ts * 100, "%")


def load_mod(name):
    return load_model(f"results/{name}")


def compare():
    emots = AVAILABLE_EMOTIONS

    scaler1 = MinMaxScaler()
    kwargss_1 = {
        "mfcc": True,
        "mel": True,
        "chroma": True
    }
    X_1, y_1 = load_data(emots, EMOTIONS_MAP, kwargss_1)
    X_1 = scaler1.fit_transform(X_1)
    input_length_1 = X_1.shape[1]

    scaler_2 = MinMaxScaler()
    kwargss_2 = {
        "mfcc": True,
        "dmfcc": True,
        "ddmfcc": True,
    }
    X_2, y_2 = load_data(emots, EMOTIONS_MAP, kwargss_2)
    X_2 = scaler_2.fit_transform(X_2)
    input_length_2 = X_2.shape[1]
    
    scaler_3 = MinMaxScaler()
    kwargss_3 = {
        "contrast": True,
        "tonnetz": True
    }
    X_3, y_3 = load_data(emots, EMOTIONS_MAP, kwargss_3)
    X_3 = scaler_3.fit_transform(X_3)
    input_length_3 = X_3.shape[1]

    dts = {
        "mfcc-mel-chroma": (X_1, y_1),
        "mfcc-dmfcc-ddmfcc": (X_2, y_2),
        "contrast-tonnetz": (X_3, y_3),
    }

    model_params = {
        'alpha': 0.01,
        'batch_size': 256,
        'epsilon': 1e-08,
        'hidden_layer_sizes': (300,),
        'learning_rate': 'adaptive',
        'max_iter': 500,
    }

    model_params_2 = {
        'alpha': 0.01,
        'batch_size': 256,
        'epsilon': 1e-08,
        'hidden_layer_sizes': (128, 256, 64,),
        'learning_rate': 'adaptive',
        'max_iter': 500,
    }

    output_dim = len(emots)
    clfs = {
        "mfcc-mel-chroma": {
            "dense-1": build_model_dense(input_length=input_length_1, output_dim=output_dim),
            "dense-2": build_model_dense2(input_length=input_length_1, output_dim=output_dim),
            "rec-1": build_model_rec(input_length=input_length_1, output_dim=output_dim),
            "mlp-1": MLPClassifier(**model_params),
            "mlp-2": MLPClassifier(**model_params_2),
        },
        "mfcc-dmfcc-ddmfcc": {
            "dense-1": build_model_dense(input_length=input_length_2, output_dim=output_dim),
            "dense-2": build_model_dense2(input_length=input_length_2, output_dim=output_dim),
            "rec-1": build_model_rec(input_length=input_length_2, output_dim=output_dim),
            "mlp-1": MLPClassifier(**model_params),
            "mlp-2": MLPClassifier(**model_params_2),
        },
        "contrast-tonnetz": {
            "dense-1": build_model_dense(input_length=input_length_3, output_dim=output_dim),
            "dense-2": build_model_dense2(input_length=input_length_3, output_dim=output_dim),
            "rec-1": build_model_rec(input_length=input_length_3, output_dim=output_dim),
            "mlp-1": MLPClassifier(**model_params),
            "mlp-2": MLPClassifier(**model_params_2),
        }
    }
    folds = 5
    results = np.zeros((len(clfs) * len(clfs["mfcc-mel-chroma"]), folds))
    confs = np.zeros((len(clfs) * len(clfs["mfcc-mel-chroma"]), folds))
    print(results.shape)
    rs = 1410

    skf = StratifiedKFold(n_splits=folds, shuffle=True, random_state=rs)

    # Results
    # mmc: dense-1
    #      dense-2
    #      mlp-1
    # mdd: dense-1
    #      dense-2
    #      mlp-2

    for fold, (train, test) in enumerate(skf.split(X_1, y_1)):
        for fts_id, (features_name, act_clfs) in enumerate(clfs.items()):
            X_all, y_all = dts[features_name]

            for clf_id, (clf_name, clf_b) in enumerate(act_clfs.items()):
                X_t, y_t = X_all[train], y_all[train]

                if "dense" in clf_name or "rec" in clf_name:
                    clf = clone_model(clf_b)
                    clf.compile(loss="categorical_crossentropy", optimizer="adam")
                    X_t_prep, X_te_prep, y_t_prep, y_te_prep = prep_tens(X_t, X_all[test], y_t, y_all[test], emotions2int)

                    clf.fit(X_t_prep, y_t_prep, batch_size=32, epochs=300, validation_data=(X_te_prep, y_te_prep))

                    y_pred = clf.predict(X_te_prep)
                    y_pred = [np.argmax(y, out=None, axis=None) for y in y_pred[0]]
                    y_test = [np.argmax(y, out=None, axis=None) for y in y_te_prep[0]]
                    acc = accuracy_score(y_test, y_pred)
                    print(f"Fold: {fold} FTS: {features_name} CLF: {clf_name} ACC: {acc}")
                    results[clf_id + fts_id * len(act_clfs.keys())] = acc

                else:
                    clf = clone(clf_b)
                    clf.fit(X_t, y_t)

                    y_pred = clf.predict(X_all[test])
                    acc = accuracy_score(y_all[test], y_pred)
                    results[clf_id + fts_id * len(act_clfs.keys()), fold] = acc
                    print(f"Fold: {fold} FTS: {features_name} CLF: {clf_name} ACC: {acc}")

    dt = datetime.now().strftime("%Y%m%d-%H%M%S")
    np.save(f"compare-{dt}", results)
    np.save(f"confs-{dt}", confs)

    return results


if __name__ == "__main__":
    kwargss = {
        "mfcc": True,
        "dmfcc": False,
        "ddmfcc": False,
        "mel": True,
        "chroma": True
    }
    # mod_dense(kwargss)
    # mod_rec(kwargss)
    mod_cl(kwargss)
    # compare()

    #cmp = np.load("compare.npy")
    #print(cmp)
