import os
import pickle  # to save model after training
from sklearn.neural_network import MLPClassifier  # multi-layer perceptron model
from sklearn.metrics import accuracy_score  # to measure how good we are

from utils import load_data

X_train, X_test, y_train, y_test = load_data(test_size=0.25)
print("Training samples # :", X_train.shape[0])
print("Testing samples  # :", X_test.shape[0])
print("Features # :", X_train.shape[1])

model_params = {
    'alpha': 0.01,
    'batch_size': 256,
    'epsilon': 1e-08,
    'hidden_layer_sizes': (300,),
    'learning_rate': 'adaptive',
    'max_iter': 500,
}

model = MLPClassifier(**model_params)
print("Training...")
model.fit(X_train, y_train)

y_pred = model.predict(X_test)

accuracy = accuracy_score(y_true=y_test, y_pred=y_pred)
print("Accuracy: {:.2f}%".format(accuracy * 100))

if not os.path.isdir("../results"):
    os.mkdir("../results")

pickle.dump(model, open("results/mlp_classifier.model", "wb"))
