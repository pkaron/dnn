import soundfile  # to read audio file
import numpy as np
import librosa  # to extract speech features
import glob
import os
import pickle  # to save model after training
from sklearn.model_selection import train_test_split  # for splitting training and testing

EMOTIONS = ["neutral", "calm", "happy", "sad", "angry", "fearful", "disgust", "surprised"]
EMOTIONS_MAP = {
    "01": EMOTIONS[0], "02": EMOTIONS[1], "03": EMOTIONS[2], "04": EMOTIONS[3],
    "05": EMOTIONS[4], "06": EMOTIONS[5], "07": EMOTIONS[6], "08": EMOTIONS[7]
}


def extract_feature(file_name, mfcc=False, chroma=False, mel=False, contrast=False, tonnetz=False):
    with soundfile.SoundFile(file_name) as sound_file:
        X = sound_file.read(dtype="float32")
        sample_rate = sound_file.samplerate
        if chroma or contrast:
            stft = np.abs(librosa.stft(X))
        result = np.array([])
        if mfcc:
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
            result = np.hstack((result, mfccs))
        if chroma:
            chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, chroma))
        if mel:
            mel = np.mean(librosa.feature.melspectrogram(y=X, sr=sample_rate).T, axis=0)
            result = np.hstack((result, mel))
        if contrast:
            contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, contrast))
        if tonnetz:
            tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
            result = np.hstack((result, tonnetz))
    return result


def load_data(test_size=0.2, random_state=7, perisist=True):
    loaded, _data = try_loading_persisted(persistence_file(random_state, test_size))
    if loaded:
        return _data["X_train"], _data["X_test"], _data["y_train"], _data["y_test"]

    X, y = [], []
    for file in glob.glob("data/ravdess-conv/Actor_*/*.wav"):
        print(f"Loading {file}")
        # get the base name of the audio file
        basename = os.path.basename(file)
        # get the emotion label
        emotion = EMOTIONS_MAP[basename.split("-")[2]]
        # extract speech features
        features = extract_feature(file, mfcc=True, chroma=True, mel=True)
        # add to data
        X.append(features)
        y.append(emotion)
    # split the data to training and testing and return it
    data = train_test_split(np.array(X), y, test_size=test_size, random_state=random_state)

    if perisist:
        persist_data(data, test_size, random_state=random_state, features=["mfcc", "chroma", "mel"])

    return data


def persist_data(data, test_size, random_state, features):
    X_tr, X_te, y_tr, y_te = data
    tostore = dict(zip(['X_train', 'X_test', 'y_train', 'y_test', "features"], [X_tr, X_te, y_tr, y_te, features]))

    if not os.path.isdir("../extracted"):
        os.mkdir("../extracted")
    with open(persistence_file(random_state, test_size), 'wb') as filestore:
        pickle.dump(tostore, filestore)  # write dict to file


def persistence_file(random_state, test_size):
    return 'extracted/REP-' + str(random_state) + "-" + str(test_size)


def try_loading_persisted(file):
    if os.path.exists(file):
        return True, load_persisted_data(file)
    else:
        return False, None


def load_persisted_data(file):
    with open(file, 'rb') as fl:
        dictionary = pickle.load(fl)

    return dictionary
