from datetime import datetime
import os
import sys
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')

import numpy as np
import pickle  # to save model after training

from sklearn.metrics import accuracy_score
from tensorflow.keras.models import Sequential
from tensorflow.python.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.python.keras.layers import LSTM, Dropout, Dense
from tensorflow.python.keras.utils.np_utils import to_categorical

from utils import load_data

AVAILABLE_EMOTIONS = {
    "neutral",
    "calm",
    "happy",
    "sad",
    "angry",
    "fearful",
    "disgust",
    "ps",  # pleasant surprised
    "boredom",
    "surprised"
}
int2emotions = {i: e for i, e in enumerate(AVAILABLE_EMOTIONS)}
emotions2int = {v: k for k, v in int2emotions.items()}


def build_model(
        input_length,
        n_rnn_layers=2,
        n_dense_layers=2,
        rnn_units=128,
        dense_units=128,
        cell=LSTM,
        dropout_mlt=0.3,
        emotions=AVAILABLE_EMOTIONS,
        loss="categorical_crossentropy",
        optimizer="adam", ):
    if n_rnn_layers < 1:
        raise Exception("n_rnn_layers must be >= 1")

    dropout = [dropout_mlt] * (n_rnn_layers + n_dense_layers)

    output_dim = len(emotions)

    model = Sequential()

    model.add(cell(rnn_units, return_sequences=True, input_shape=(None, input_length)))
    model.add(Dropout(dropout[0]))

    i = 0
    for i in range(1, n_rnn_layers):
        model.add(cell(rnn_units, return_sequences=True))
        model.add(Dropout(dropout[i]))

    for j in range(n_dense_layers):
        model.add(Dense(dense_units))
        model.add(Dropout(dropout[i + j]))

    # Classification
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def prepare_data(test_size=0.2, random_state=7, perisist=True):
    X_train, X_test, y_train, y_test = load_data(test_size, random_state, perisist)
    # X_train, X_test, y_train, y_test = _data["X_train"], _data["X_test"], _data["y_train"], _data["y_test"]

    X_train_shape = X_train.shape
    X_test_shape = X_test.shape

    X_train = X_train.reshape((1, X_train_shape[0], X_train_shape[1]))
    X_test = X_test.reshape((1, X_test_shape[0], X_test_shape[1]))

    y_train = to_categorical([emotions2int[str(e)] for e in y_train])
    y_test = to_categorical([emotions2int[str(e)] for e in y_test])

    y_train_shape = y_train.shape
    y_test_shape = y_test.shape

    y_train = y_train.reshape((1, y_train_shape[0], y_train_shape[1]))
    y_test = y_test.reshape((1, y_test_shape[0], y_test_shape[1]))

    input_length = X_train[0].shape[1]
    return {
        "X_train": X_train,
        "X_test": X_test,
        "y_train": y_train,
        "y_test": y_test,
        "input_length": input_length
    }


def train(_model, _data, batch_size=64, epochs=300):

    model_filename = "results/" + _model.name + datetime.now().strftime("%Y%m%d-%H%M%S")
    checkpointer = ModelCheckpoint(model_filename, save_best_only=True, verbose=1)

    log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)
    
    return _model.fit(
        _data["X_train"], _data["y_train"],
        batch_size=batch_size,
        epochs=epochs,
        validation_data=(_data["X_test"], _data["y_test"]),
        callbacks=[tensorboard_callback, checkpointer],
        verbose=True
    )


def test_score(_model, _data):
    X_train, X_test, y_train, y_test = data["X_train"], data["X_test"], data["y_train"], data["y_test"]
    y_test = y_test[0]
    y_pred = _model.predict(X_test)[0]
    y_pred = [np.argmax(y, out=None, axis=None) for y in y_pred]
    y_test = [np.argmax(y, out=None, axis=None) for y in y_test]
    return accuracy_score(y_true=y_test, y_pred=y_pred)


if __name__ == "__main__":
    data = prepare_data()
    model = build_model(data["input_length"])
    hist = train(model, data)
    ts = test_score(model, data)
    print("Test accuracy score:", ts * 100, "%")






























































































































































































































