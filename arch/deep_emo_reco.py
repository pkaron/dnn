import os
# disable keras loggings
import sys

stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import tensorflow as tf

from tensorflow.keras.layers import LSTM, GRU, Dense, Activation, LeakyReLU, Dropout
from tensorflow.keras.layers import Conv1D, MaxPool1D, GlobalAveragePooling1D
from tensorflow.keras.models import Sequential
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.keras.utils import to_categorical

from sklearn.metrics import accuracy_score, mean_absolute_error, confusion_matrix

from data_extractor import load_data
from emotion_recognition import EmotionRecognizer

import numpy as np
import pandas as pd
import random

EMOTIONS = ["neutral", "calm", "happy", "sad", "angry", "fearful", "disgust", "surprised"]
EMOTIONS_MAP = {
    "01": EMOTIONS[0], "02": EMOTIONS[1], "03": EMOTIONS[2], "04": EMOTIONS[3],
    "05": EMOTIONS[4], "06": EMOTIONS[5], "07": EMOTIONS[6], "08": EMOTIONS[7]
}


class DeepEmoReco(EmoReco):
    def __init__(self, **kwargs):
        self.n_rnn_layers = kwargs.get("n_rnn_layers", 2)
        self.n_dense_layers = kwargs.get("n_dense_layers", 2)
        self.rnn_units = kwargs.get("rnn_units", 128)
        self.dense_units = kwargs.get("dense_units", 128)
        self.cell = kwargs.get("cell", LSTM)

        # list of dropouts of each layer
        # must be len(dropouts) = n_rnn_layers + n_dense_layers
        self.dropout = kwargs.get("dropout", 0.3)
        self.dropout = self.dropout if isinstance(self.dropout, list) else [self.dropout] * (
                self.n_rnn_layers + self.n_dense_layers)
        # number of classes ( emotions )
        self.output_dim = len(self.emotions)

        # optimization attributes
        self.optimizer = kwargs.get("optimizer", "adam")
        self.loss = kwargs.get("loss", "categorical_crossentropy")

        # training attributes
        self.batch_size = kwargs.get("batch_size", 64)
        self.epochs = kwargs.get("epochs", 500)

        # the name of the model
        self._update_model_name()

        self.model = None
        self._compute_input_length()
        self.model_created = False

    def _update_model_name(self):
        emotions_str = "".join(sorted([e[0].upper() for e in self.emotions]))
        problem_type = 'cls' if self.classification else 'reg'
        dropout_str = get_dropout_str(self.dropout, n_layers=self.n_dense_layers + self.n_rnn_layers)
        self.model_name = f"{emotions_str}-{problem_type}-{self.cell.__name__}-layers-{self.n_rnn_layers}-{self.n_dense_layers}-units-{self.rnn_units}-{self.dense_units}-dropout-{dropout_str}.h5"

    def _compute_input_length(self):
        if not self.data_loaded:
            self.load_data()
        self.input_length = self.X_train[0].shape[1]

    def load_data(self):
        """
        Loads and extracts features from the audio files for the db's specified
        """
        if not self.data_loaded:
            result = load_data(self.train_desc_files, self.test_desc_files, self.audio_config, self.classification,
                                emotions=self.emotions, balance=self.balance)
            self.X_train = result['X_train']
            self.X_test = result['X_test']
            self.y_train = result['y_train']
            self.y_test = result['y_test']
            self.train_audio_paths = result['train_audio_paths']
            self.test_audio_paths = result['test_audio_paths']
            self.balance = result["balance"]
            if self.verbose:
                print("Data loaded")
            self.data_loaded = True


def get_dropout_str(dropout, n_layers=3):
    if isinstance(dropout, list):
        return "_".join([str(d) for d in dropout])
    elif isinstance(dropout, float):
        return "_".join([str(dropout) for i in range(n_layers)])
