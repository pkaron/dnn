import glob
import pickle
from datetime import datetime
import os

import seaborn as sns
import librosa
import soundfile
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

# stderr = sys.stderr
# sys.stderr = open(os.devnull, 'w')

import numpy as np

from sklearn.metrics import accuracy_score, confusion_matrix
from tensorflow.keras.models import Sequential
from tensorflow.python.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.python.keras.layers import LSTM, Dropout, Dense
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.regularizers import L2
from tensorflow.python.keras.utils.np_utils import to_categorical

EMOTIONS = ["neutral", "calm", "happy", "sad", "angry", "fearful", "disgust", "surprised"]
EMOTIONS_MAP = {
    "01": EMOTIONS[0], "02": EMOTIONS[1], "03": EMOTIONS[2], "04": EMOTIONS[3],
    "05": EMOTIONS[4], "06": EMOTIONS[5], "07": EMOTIONS[6], "08": EMOTIONS[7]
}

AVAILABLE_EMOTIONS = {
    "neutral",
    "calm",
    "happy",
    "sad",
    "angry",
    "fearful",
    "disgust",
    "surprised"
}
int2emotions = {i: e for i, e in enumerate(AVAILABLE_EMOTIONS)}
emotions2int = {v: k for k, v in int2emotions.items()}

SAMPLE_RATE = 16_000


def persistence_file(random_state, test_size):
    return 'extracted/REP-' + str(random_state) + "-" + str(test_size)

def persist_data(data, test_size, random_state, features):
    X_tr, X_te, y_tr, y_te = data
    tostore = dict(zip(['X_train', 'X_test', 'y_train', 'y_test', "features"], [X_tr, X_te, y_tr, y_te, features]))

    if not os.path.isdir("../extracted"):
        os.mkdir("../extracted")
    with open(persistence_file(random_state, test_size), 'wb') as filestore:
        pickle.dump(tostore, filestore)  # write dict to file


def get_raw_files():
    return glob.glob("data/ravdess-conv-trim/Actor_*/*.wav")


def trim_silence(file):
    f, sr = librosa.load(file, sr=SAMPLE_RATE)
    tr, idx = librosa.effects.trim(f)
    return tr


def save_wav(audio_series, path):
    soundfile.write(path, audio_series, SAMPLE_RATE, format='wav')


def extract_feature(file_name, mfcc=False, chroma=False, mel=False, contrast=False, tonnetz=False):
    with soundfile.SoundFile(file_name) as sound_file:
        X = sound_file.read(dtype="float32")
        sample_rate = sound_file.samplerate
        if chroma or contrast:
            stft = np.abs(librosa.stft(X))
        result = np.array([])
        if mfcc:
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20).T, axis=0)
            result = np.hstack((result, mfccs))
            #dmfccs = np.mean(librosa.feature.delta(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20)).T, axis=0)
            #result = np.hstack((result, dmfccs))
            #ddmfccs = np.mean(librosa.feature.delta(librosa.feature.delta(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20))).T, axis=0)
            #result = np.hstack((result, ddmfccs))
        if chroma:
            chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, chroma))
        if mel:
            mel = np.mean(librosa.feature.melspectrogram(y=X, sr=sample_rate).T, axis=0)
            result = np.hstack((result, mel))
        if contrast:
            contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, contrast))
        if tonnetz:
            tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
            result = np.hstack((result, tonnetz))
    return result


def load_data(test_size, random_state=7, perisist=False):
    #loaded, _data = try_loading_persisted(persistence_file(random_state, test_size))
    #if loaded:
    #    return _data["X_train"], _data["X_test"], _data["y_train"], _data["y_test"]

    X, y = [], []
    ctr = 0
    for file in get_raw_files():
        # if ctr > 30:
        #    continue
        # ctr +=1

        print(f"Loading {file}")
        # get the base name of the audio file
        basename = os.path.basename(file)
        # get the emotion label
        emotion_num = basename.split("-")[2]
        emotion = EMOTIONS_MAP[emotion_num]
        # extract speech features
        features = extract_feature(file, mfcc=True, chroma=False, mel=False)
        # add to data
        X.append(features)
        y.append(emotion)
    # split the data to training and testing and return it

    scaler = MinMaxScaler()
    X = scaler.fit_transform(X)
    data = train_test_split(np.array(X), y, test_size=test_size, random_state=random_state)

    if perisist:
        persist_data(data, test_size, random_state=random_state, features=["mfcc", "chroma", "mel"])

    return data


def build_model_rec(
        input_length,
        n_rnn_layers=2,
        n_dense_layers=2,
        rnn_units=128,
        dense_units=128,
        cell=LSTM,
        dropout_mlt=0.3,
        emotions=AVAILABLE_EMOTIONS,
        loss="categorical_crossentropy",
        optimizer="adam", ):
    if n_rnn_layers < 1:
        raise Exception("n_rnn_layers must be >= 1")

    dropout = [dropout_mlt] * (n_rnn_layers + n_dense_layers)

    output_dim = len(emotions)

    model = Sequential()

    model.add(cell(rnn_units, return_sequences=True, input_shape=(None, input_length)))
    model.add(Dropout(dropout[0]))

    i = 0
    for i in range(1, n_rnn_layers):
        model.add(cell(rnn_units, return_sequences=True))
        model.add(Dropout(dropout[i]))

    for j in range(n_dense_layers):
        model.add(Dense(dense_units))
        model.add(Dropout(dropout[i + j]))

    # Classification
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def build_model(
        input_length,
        emotions=AVAILABLE_EMOTIONS,
        loss="categorical_crossentropy",
        optimizer="adam" ):

    output_dim = len(emotions)

    model = Sequential()
    model.add(Dense(256, kernel_regularizer=L2(l2=1e-4), input_shape=(None, input_length)))
    model.add(Dense(512, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(64, kernel_regularizer=L2(l2=1e-4)))
    model.add(Dense(output_dim, activation="softmax"))
    model.compile(loss=loss, metrics=["accuracy"], optimizer=optimizer)

    return model


def prepare_data(test_size=0.15, random_state=7, perisist=True):
    X_train, X_test, y_train, y_test = load_data(test_size, random_state, perisist)
    # X_train, X_test, y_train, y_test = _data["X_train"], _data["X_test"], _data["y_train"], _data["y_test"]
    print(y_train)
    y_test_raw = y_test
    X_train_shape = X_train.shape
    X_test_shape = X_test.shape

    X_train = X_train.reshape((1, X_train_shape[0], X_train_shape[1]))
    X_test = X_test.reshape((1, X_test_shape[0], X_test_shape[1]))

    y_train = to_categorical([emotions2int[str(e)] for e in y_train])
    y_test = to_categorical([emotions2int[str(e)] for e in y_test])

    y_train_shape = y_train.shape
    y_test_shape = y_test.shape

    y_train = y_train.reshape((1, y_train_shape[0], y_train_shape[1]))
    y_test = y_test.reshape((1, y_test_shape[0], y_test_shape[1]))

    input_length = X_train[0].shape[1]
    return {
        "X_train": X_train,
        "X_test": X_test,
        "y_train": y_train,
        "y_test": y_test,
        "input_length": input_length,
        "y_test_raw": y_test_raw
    }


def train(_model, _data, batch_size=10, epochs=300):
    model_filename = "results/" + _model.name + datetime.now().strftime("%Y%m%d-%H%M%S")
    checkpointer = ModelCheckpoint(model_filename, save_best_only=True, verbose=1)

    log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

    return _model.fit(
        _data["X_train"], _data["y_train"],
        batch_size=batch_size,
        epochs=epochs,
        validation_data=(_data["X_test"], _data["y_test"]),
        callbacks=[tensorboard_callback, checkpointer],
        verbose=True
    )

def test_score_name(name):
    model = load_mod(name)
    data = prepare_data()
    test_score(model, data)

def test_score(_model, _data):
    X_train, X_test, y_train, y_test = _data["X_train"], _data["X_test"], _data["y_train"], _data["y_test"]
    y_test = y_test[0]
    y_pred = _model(X_test)[0]
    y_pred = [np.argmax(y, out=None, axis=None) for y in y_pred]
    y_test = [np.argmax(y, out=None, axis=None) for y in y_test]

    cff = confusion_matrix(y_test, y_pred)
    score = accuracy_score(y_true=y_test, y_pred=y_pred)
    print(f"Accuracy: {score}%")
    cm = sns.light_palette("seagreen", as_cmap=True)
    sns.heatmap(cff, annot=True, fmt='d', cmap=cm, xticklabels=EMOTIONS, yticklabels=EMOTIONS)
    plt.show()
    return score


def mod():
    data = prepare_data()
    model = build_model(data["input_length"])
    hist = train(model, data)
    print(model.summary())
    ts = test_score(model, data)
    print("Test accuracy score:", ts * 100, "%")


def conf(mod_name):
    data = prepare_data()
    model = load_mod(mod_name)
    X_train, X_test, y_train, y_test = data["X_train"], data["X_test"], data["y_train"], data["y_test"]
    pred = model(X_test)
    labels = []
    for y in y_test[0]:
        labels.append(EMOTIONS[np.argmax(y)])

    preds = []
    for p in pred[0]:
        arg_max = np.argmax(p)
        preds.append(EMOTIONS[arg_max])

    cff = confusion_matrix(labels, preds)
    score = accuracy_score(y_true=labels, y_pred=preds)
    print(f"Accuracy: {score}%")
    cm = sns.light_palette("seagreen", as_cmap=True)
    sns.heatmap(cff, annot=True, fmt='d', cmap=cm, xticklabels=EMOTIONS, yticklabels=EMOTIONS)
    plt.show()


def prep():
    for f in get_raw_files():
        spl = f.split("/")
        new_pth = f"{spl[0]}/ravdess-conv-trim/{spl[2]}/{spl[3]}"
        print(new_pth)
        # os.makedirs(f"{spl[0]}/ravdess-conv-trim/{spl[2]}", exist_ok=True)
        x = trim_silence(f)
        save_wav(x, new_pth)


def load_mod(name):
    return load_model(f"results/{name}")


if __name__ == "__main__":
    curr = "seq-4-dense"
    #mod()
    #conf("seq-4-dense-delta-mfcc")

    conf(curr)
    test_score_name(curr)
