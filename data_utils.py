import glob
import os

import librosa
import numpy as np
import soundfile
from keras.utils.np_utils import to_categorical
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

SAMPLE_RATE = 16_000


def join_if(base, cond, string):
    if cond:
        return base.join(string)
    else:
        return base


def persistence_file(input_file, **kwargs):
    split = input_file.split("/")
    basename = split[-1]
    actor = split[-2]

    dr = ""
    for k, v in kwargs.items():
        if v:
            dr = dr + k + "-"

    dr = dr[:-1]  # remove last dash

    dir_only = f"extracted2/{dr}/{actor}"
    full = f"{dir_only}/{basename}"
    return dir_only, full


def persist_data(data, input_file, **kwargs):
    dir_only, full = persistence_file(input_file, **kwargs)
    os.makedirs(dir_only, exist_ok=True)
    with open(full, 'wb') as fil:
        np.save(fil, data)


def get_raw_files():
    return glob.glob("data/ravdess-conv-trim/Actor_*/*.wav")


def extract_feature(file_name, **kwargs):
    mfcc = kwargs.get("mfcc")
    dmfcc = kwargs.get("dmfcc")
    ddmfcc = kwargs.get("ddmfcc")
    chroma = kwargs.get("chroma")
    mel = kwargs.get("mel")
    contrast = kwargs.get("contrast")
    tonnetz = kwargs.get("tonnetz")

    with soundfile.SoundFile(file_name) as sound_file:
        X = sound_file.read(dtype="float32")
        sample_rate = sound_file.samplerate
        if chroma or contrast:
            stft = np.abs(librosa.stft(X))
        result = np.array([])
        if mfcc:
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20).T, axis=0)
            result = np.hstack((result, mfccs))
        if dmfcc:
            dmfccs = np.mean(librosa.feature.delta(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20)).T, axis=0)
            result = np.hstack((result, dmfccs))
        if ddmfcc:
            ddmfccs = np.mean(
                librosa.feature.delta(librosa.feature.delta(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=20))).T,
                axis=0)
            result = np.hstack((result, ddmfccs))
        if chroma:
            chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, chroma))
        if mel:
            mel = np.mean(librosa.feature.melspectrogram(y=X, sr=sample_rate).T, axis=0)
            result = np.hstack((result, mel))
        if contrast:
            contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
            result = np.hstack((result, contrast))
        if tonnetz:
            tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
            result = np.hstack((result, tonnetz))
    return result


def try_loading_extracted(file, **kwargs):
    dir_only, full = persistence_file(file, **kwargs)

    if os.path.exists(full):
        print(f"Loading pre-saved file {file}")
        return np.load(full)
    else:
        return None


def load_data(emotions, emotions_map, kwargs):
    X, y = [], []
    ctr = 0
    for file in get_raw_files():
        # if ctr > 4:
        #     continue
        # ctr += 1

        print(f"Loading {file}")
        # get the base name of the audio file
        basename = os.path.basename(file)
        emotion_num = basename.split("-")[2]
        emotion = emotions_map[emotion_num]

        if emotion not in emotions:
            continue

        fts = try_loading_extracted(file, **kwargs)
        if fts is None:
            fts = extract_feature(file, **kwargs)
            persist_data(fts, file, **kwargs)

        X.append(fts)
        y.append(emotion)

    return np.array(X), np.array(y)


def prepare_data_tens(X, y, emotions2int, test_size=0.15, random_state=None, normalize=False):
    if normalize:
        scaler = MinMaxScaler()
        X = scaler.fit_transform(X)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)

    return prep_tens(X_train, X_test, y_train, y_test, emotions2int)


def prep_tens(X_train, X_test, y_train, y_test, emotions2int):
    X_train = X_train.reshape((1,  X_train.shape[0],  X_train.shape[1]))
    X_test = X_test.reshape((1, X_test.shape[0], X_test.shape[1]))

    y_train = to_categorical([emotions2int[str(e)] for e in y_train])
    y_test = to_categorical([emotions2int[str(e)] for e in y_test])

    y_train = y_train.reshape((1, y_train.shape[0], y_train.shape[1]))
    y_test = y_test.reshape((1, y_test.shape[0], y_test.shape[1]))

    return X_train, X_test, y_train, y_test


def prepare_data_cl(X, y, test_size=0.15, random_state=None, normalize=False):
    if normalize:
        scaler = MinMaxScaler()
        X = scaler.fit_transform(X)

    return train_test_split(X, y, test_size=test_size, random_state=random_state)


if __name__ == "__main__":

    file_name_conv_trim_neutral = "data/ravdess-conv-trim/Actor_01/03-01-01-01-01-02-01.wav"
    file_name_conv_trim_happy = "data/ravdess-conv-trim/Actor_01/03-01-03-01-01-02-01.wav"
    file_name_conv = 'data/ravdess-conv/Actor_01/03-01-01-01-01-01-01.wav'
    file_name_raw = 'data/ravdess-raw/Actor_01/03-01-01-01-01-01-01.wav'

    raw = soundfile.SoundFile(file_name_raw)
    conv = soundfile.SoundFile(file_name_conv)
    conv_trim_neutral = soundfile.SoundFile(file_name_conv_trim_neutral)
    conv_trim_happy = soundfile.SoundFile(file_name_conv_trim_happy)

    conv_trim_a = conv_trim_neutral.read(dtype="float32")
    conv_trim_b = conv_trim_happy.read(dtype="float32")
    sr = conv_trim_neutral.samplerate
    print(sr)
    # mel = librosa.feature.melspectrogram(y=conv_trim_a, sr=sr, n_fft=2048)
    # melh = librosa.feature.melspectrogram(y=conv_trim_b, sr=sr, n_fft=2048)
    mel = librosa.feature.mfcc(y=conv_trim_a, sr=sr, n_mfcc=20)
    melh = librosa.feature.delta(librosa.feature.mfcc(y=conv_trim_a, sr=sr, n_mfcc=20))
    M_db = mel
    M_dba = melh
    import librosa.display
    fig, ax = plt.subplots(nrows=2, sharex=True)
    img = librosa.display.specshow(M_db, x_axis='time', ax=ax[0])
    img2 = librosa.display.specshow(M_dba, x_axis='time', ax=ax[1])

    ax[0].set_title("Neutral | MFCC")
    ax[1].set_title("Neutral | Delta MFCC")

    fig.colorbar(img, ax=[ax[0]])
    fig.colorbar(img2, ax=[ax[1]])

    plt.show()

    # fig, ax = plt.subplots(3, 1)
    # ax[0].plot(raw.read(dtype="float32"))
    # ax[0].set_title("RAW")
    # ax[1].plot(conv.read(dtype="float32"))
    # ax[1].set_title("Sampled 16kHz")
    # ax[2].plot(conv_trim.read(dtype="float32"))
    # ax[2].set_title("Sampled, trimmed")
    # plt.show()



    raw.close()
    conv.close()
    conv_trim_neutral.close()
